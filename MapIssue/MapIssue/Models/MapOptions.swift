//
//  MapOptions.swift
//  WeatherNation
//
//  Created by solodkiy on 11.04.2022.
//

import Foundation

public enum MapOptions {

    case back
    case cancel
    case play
    case pause
    case appearance
    case time

}

// MARK: Extensions

extension MapOptions {

    func getLiteral() -> String {
        return ""
    }

    func getImageLiteral() -> String? {
        switch self {
        case .back:
            return "back"
        case .cancel:
            return "ic_close"
        case .appearance:
            return "Filter"
        case .play:
            return "mapPlay"
        case .pause:
            return "mapStop"
        case .time:
            return "upArrow"
        }
    }

    func getTintColorLiteral() -> String {
        switch self {
        case .back, .cancel, .appearance, .play, .pause, .time:
            return "feed.cell.title.textColor"
        }
    }

    func getBackgroundColorLiteral() -> String {
        switch self {
        case .cancel:
            return "watermelon"
        case .play, .pause, .back, .appearance, .time:
            return "search.location.button.background"
        }
    }

}
