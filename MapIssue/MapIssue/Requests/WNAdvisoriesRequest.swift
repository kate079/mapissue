//
//  WNAdvisoriesRequest.swift
//  WeatherNation
//
//  Created by Misha on 04.05.2022.
//

import Foundation

struct WNAdvisoriesRequest: Encodable {

    // MARK: - Response
    struct Response: Codable {
        let id: String?
        let loc: LOC
        let dataSource: String?
        let details: Details
        let timestamps: Timestamps
        let poly: String?
        let geoPoly: GeoPoly?
        let includes: Includes
        let place: Place
        let profile: Profile
        let active: Bool
    }

    // MARK: - Details
    struct Details: Codable {
        let type, name, loc, emergency: String?
        let priority: Double?
        let color, cat, body, bodyFull: String?
    }

    // MARK: - Includes
    struct Includes: Codable {
        let wxzones: [String]
        let fips, zipcodes, counties: [String?]
    }
    
    // MARK: - GeoPoly
    struct GeoPoly: Codable {
        let coordinates: [[[Double]]]
    }

    // MARK: - LOC
    struct LOC: Codable {
        let long, lat: Double?
    }

    // MARK: - Place
    struct Place: Codable {
        let name, state, country: String?
    }

    // MARK: - Profile
    struct Profile: Codable {
        let tz: String?
    }

    // MARK: - Timestamps
    struct Timestamps: Codable {
        let issued: Int?
        let issuedISO: String?
        let begins: Int?
        let beginsISO: String?
        let expires: Int?
        let expiresISO: String?
        let updated: Int?
        let updateISO: String?
        let added: Int?
        let addedISO: String?
        let created: Int?
        let createdISO: String?
    }
    
    class Input: Encodable {
        
        let params: [String: String]
        let path: String
        
        init(path: String, params: [String: String]) {
            self.params = params
            self.path = path
        }
    }
    
    class CoordinateInput: Input {
        init(location: WNLocation) {
            super.init(
                path: "services/iphone/advisories/closest",
                params: ["p": "\(location.coordinate.lat),\(location.coordinate.long)"]
            )
        }
    }
        
    private let input: Input
    
    init(location: WNLocation) {
        self.input = CoordinateInput(location: location)
    }
}

extension WNAdvisoriesRequest: NetworkRequest {
    typealias Body = WNAdvisoriesRequest
    
    var url: URL {
        return Environment.baseURL.appendingPathComponent(input.path)
    }
    
    var method: HTTPMethod {
        return .get
    }
    
    var headers: [String: String] {
        return [:]
    }
    
    var params: [String: String] {
        return input.params
    }
    
    var body: Body? {
        return nil
    }
}
