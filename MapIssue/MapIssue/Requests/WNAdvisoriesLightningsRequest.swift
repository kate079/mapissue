//
//  WNAdvisoriesLightningsRequest.swift
//  WeatherNation
//
//  Created by natife on 08.07.2022.
//

import Foundation

struct WNAdvisoriesLightningsRequest: Encodable {

    // MARK: - Response
    struct Response: Codable {
        let id: String?
        let loc: Location
    }

    // MARK: - LOC
    struct Location: Codable {
        let long, lat: Double?
    }

    class Input: Encodable {

        let params: [String: String]
        let path: String

        init(path: String, params: [String: String]) {
            self.params = params
            self.path = path
        }
    }

    class CoordinateInput: Input {
        init(location: WNLocation,
             distance: Int) {
            super.init(
                path: "lightning/\(location.coordinate.lat),\(location.coordinate.long)",
                params: ["radius": "\(distance)miles",
                         "client_id": Environment.aerisApiKey,
                         "client_secret": Environment.aerisSecretKey]
            )
        }
    }

    private let input: Input

    init(location: WNLocation,
         distance: Int) {
        self.input = CoordinateInput(location: location, distance: distance)
    }
}

extension WNAdvisoriesLightningsRequest: NetworkRequest {
    typealias Body = WNAdvisoriesRequest

    var url: URL {
        return Environment.aerisURL.appendingPathComponent(input.path)
    }

    var method: HTTPMethod {
        return .get
    }

    var headers: [String: String] {
        return [:]
    }

    var params: [String: String] {
        return input.params
    }

    var body: Body? {
        return nil
    }
}

