//
//  Date+UnixConverter.swift
//  WeatherNation
//
//  Created by Misha on 21.02.2022.
//

import Foundation
import QuartzCore

extension Date {

    func dToString() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "hh:mm a"
        dateFormatter.timeZone = .current
        return dateFormatter.string(from: self)
    }
}
