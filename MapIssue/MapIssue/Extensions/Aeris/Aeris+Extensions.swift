//
//  Aeris+Extensions.swift
//  WeatherNation
//
//  Created by natife on 05.07.2022.
//

import AerisWeatherKit
import AerisMapKit

extension AWFRasterMapLayer {
    func includeTextDataIfPossible() {
        switch layerType {
        case .feelsLike, .windSpeeds, .dewPoints, .heatIndex:
            includeTextData = true
        default: includeTextData = false
        }
    }
}
