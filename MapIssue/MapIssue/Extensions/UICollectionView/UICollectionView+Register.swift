//
//  UICollectionView+register.swift
//  WeatherNation
//
//  Created by Misha on 15.02.2022.
// swiftlint:disable all force_cast

import UIKit

extension UICollectionView {
    func registerNib<T: UICollectionViewCell>(for type: T.Type) {
        self.register(type.nib, forCellWithReuseIdentifier: type.identifier)
    }

    func deque<T: UICollectionViewCell>(type: T.Type, indexPath: IndexPath) -> T {
        return self.dequeueReusableCell(
            withReuseIdentifier: type.identifier,
            for: indexPath
        ) as! T
    }
}
