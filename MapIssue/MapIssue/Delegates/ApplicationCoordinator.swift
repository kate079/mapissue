//
//  ApplicationCoordinator.swift
//  WeatherNation
//
//  Created by Aleksey Shaforostov on 01.11.2021.
//

import UIKit

final class ApplicationCoordinator {
    
    // MARK: - Public methods
    
    func start() {
        startServices()
        navigationController.isNavigationBarHidden = true
        window?.rootViewController = navigationController
        window?.makeKeyAndVisible()
		let mapCoordinator = MapCoordinator(
            navigationController: navigationController,
            location: WNLocation(coordinate: coordinates))
        mapCoordinator.start()
    }
    
    // MARK: - Private properties
    
    private unowned let window: UIWindow?
    private let navigationController = UINavigationController()
    private let coordinates = WNCoordinate(lat: 37.773972, long: -122.431297)

    private lazy var urlSession = URLSession.shared
    private lazy var encoder = JSONEncoder()
    private lazy var decoder = JSONDecoder()
    private lazy var networkService: NetworkService = NetworkServiceImpl(session: urlSession, encoder: encoder)
    private lazy var aerisService: AerisWeatherService = AerisWeatherServiceImpl(networkService: networkService, decoder: decoder)

    // MARK: - Init
    
    init(window: UIWindow?) {
        self.window = window
    }
    
    private func startServices() {
        aerisService.start()
    }
}
