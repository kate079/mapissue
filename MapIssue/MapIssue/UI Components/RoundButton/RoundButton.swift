//
//  RoundButton.swift
//  WeatherNation
//
//  Created by solodkiy on 09.04.2022.
//

import UIKit

class RoundButton: UIButton {

    public var selfType: MapOptions = .back {
        didSet {
            configureUI()
        }
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        configureUI()
    }

    private func configureUI() {
        layer.cornerRadius = frame.height / 2
        setTitle(selfType.getLiteral(), for: .normal)
        if let imageString = selfType.getImageLiteral() {
            setImage(UIImage(named: imageString), for: .normal)
        }
        tintColor = UIColor(named: selfType.getTintColorLiteral())
        backgroundColor = UIColor(named: selfType.getBackgroundColorLiteral())
    }

}
