//
//  RadioButton.swift
//  WeatherNation
//
//  Created by Micle on 08.02.2022.
//

import UIKit

class RadioButton: UIButton {

	public var radioButtonStack: [RadioButton] = []

	public func setSelected() {
		self.isSelected = true
		radioButtonStack.forEach({ button in
			button.isSelected = false
		})
	}

	required init?(coder: NSCoder) {
		super.init(coder: coder)
		selectedAppearance()
	}

	private func selectedAppearance() {
		self.setImage(UIImage(named: "radioButtonSelected"), for: .selected)
		self.setImage(UIImage(named: "radioButtonDisabled"), for: .normal)
	}

}
