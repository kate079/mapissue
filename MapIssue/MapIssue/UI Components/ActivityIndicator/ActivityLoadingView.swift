//
//  ActivityLoadingView.swift
//  WeatherNation
//
//  Created by Alex Hafros on 05.11.2021.
//

import UIKit

class ActivityLoadingView: UIView {

    private var color: UIColor
    private var scaleFactor: CGFloat

    private let activityIndicatorView = UIActivityIndicatorView()

    var hideWhenStopped: Bool = true {
        didSet {
            activityIndicatorView.hidesWhenStopped = hideWhenStopped
        }
    }

    /// Fires activity indicator animation
    var loading: Bool = true {
        didSet {
            if loading {
                startAnimating()
            } else {
                stopAnimating()
            }
        }
    }

    var style: UIActivityIndicatorView.Style = .large {
        didSet {
            activityIndicatorView.style = style
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.color = .white
        scaleFactor = 1.0
        self.setupUI()
    }

    required init?(coder: NSCoder) {
        self.color = .white
        scaleFactor = 1.0
        super.init(coder: coder)
        self.setupUI()
    }

    private func setupUI() {
        activityIndicatorView.style = style
        activityIndicatorView.hidesWhenStopped = hideWhenStopped
        activityIndicatorView.color = color
        activityIndicatorView.transform = .init(scaleX: scaleFactor, y: scaleFactor)
        startAnimating()
        self.addSubview(activityIndicatorView)
        activityIndicatorView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            activityIndicatorView.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            activityIndicatorView.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        ])
    }

    func startAnimating() {
        activityIndicatorView.startAnimating()
    }

    func stopAnimating() {
        activityIndicatorView.stopAnimating()
    }
}
