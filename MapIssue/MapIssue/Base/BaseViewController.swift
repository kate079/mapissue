//
//  BaseViewController.swift
//  oxy-ios
//
//  Created by Aleksey Shaforostov on 01.09.2021.
//

import UIKit

class BaseViewController<ViewModel>: UIViewController {

    // MARK: - Public properties

    var viewModel: ViewModel

    // MARK: - Public methods

    func observeViewModelState() {}

    // MARK: - Init

    init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: String(describing: Self.self), bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemGray6
        observeViewModelState()
    }
}
