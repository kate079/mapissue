//
//  BaseCollectionViewCell.swift
//  WeatherNation
//
//  Created by Misha on 28.02.2022.
//

import UIKit

class BaseCollecitionViewCell<Model>: UICollectionViewCell {

    // MARK: - internal methods -

    func didSelect() {
        backgroundColor = selectedColor
    }

    func didDeselect() {
        backgroundColor = deselectedColor
    }

    // MARK: - Private property -

    private let deselectedColor = UIColor(named: "prussia1")
    private let selectedColor = UIColor(named: "searchLocationButtonBackground")

    // MARK:  - Lifecycle -

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func prepareForReuse() {
        backgroundColor = deselectedColor
    }

    func configure(with model: Model) {
        fatalError("Method must be overwritted in child class")
    }
}
