//
//  Reusable.swift
//  WeatherNation
//
//  Created by Alex Hafros on 05.11.2021.
//

import UIKit

protocol Reusable {}

extension Reusable {
    static var identifier: String {
        return String(describing: self)
    }
}

extension Reusable where Self: UICollectionViewCell {
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: Bundle(for: self))
    }
}

extension UIView: Reusable {}
extension UIViewController: Reusable {}
