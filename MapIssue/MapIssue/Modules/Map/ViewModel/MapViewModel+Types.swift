//
//  MapViewModel+Types.swift
//  WeatherNation
//
//  Created by solodkiy on 06.04.2022.
//

import Foundation
import UIKit
import AerisMapKit

extension MapViewModel {
    
    enum ResultType {
        case fullUpdate
        case overlayUpdate
    }
    
    enum State {
        case idle
        case loading
        case failure
        case started(
            _ currentLayer: AWFMapLayer
        )
        case updatedOverlay(
            _ currentLayer: AWFMapLayer
        )
    }
    
    struct TypeItem {
        let name: TypeItemNameEnum
        let isSelected: Bool
        let mapLayer: AWFMapLayer
        
        init(name: TypeItemNameEnum,
             isSelected: Bool = false,
             mapLayer: AWFMapLayer) {
            self.name = name
            self.isSelected = isSelected
            self.mapLayer = mapLayer
        }
        
    }
    
    enum TypeItemNameEnum {
        case radar,
             infraredSatellite,
             visibleSatellite,
             advisories,
             temperatures,
             winds,
             dewPoints,
             humidity,
             windChill,
             heatIndex,
             lighting
        
        var name: String {
            switch self {
            case .radar: return "Radar"
            case .infraredSatellite: return "Infrared Satellite"
            case .visibleSatellite: return "Visible Satellite"
            case .advisories: return "Advisories"
            case .temperatures: return "Current Temperatures"
            case .winds: return "Current Winds"
            case .dewPoints: return "Current Dew Points"
            case .humidity: return "Current Humidity"
            case .windChill: return "Current Wind Chill"
            case .heatIndex: return "Current Heat Index"
            case .lighting: return "Lightning"
            }
        }
    }
}
