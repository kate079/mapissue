//
//  MapViewModel.swift
//  WeatherNation
//
//  Created by solodkiy on 06.04.2022.
//

import Foundation
import AerisWeatherKit
import AerisMapKit

final class MapViewModel {
    
    // MARK: - Public properties
    
    var state: GTObservable<State>
    
    var location: WNLocation {
        _location
    }
    
    var userCurrentLocation: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(
            latitude: _userCurrentLocation?.lat ?? homeLocation.coordinate.lat,
            longitude: _userCurrentLocation?.long ?? homeLocation.coordinate.long
        )
    }
    
    // MARK: - Public methods
        
    func isMetric() -> Bool {
        return false
    }
    
    public func start() {
        state.value = .loading
        createData()
        updatedTypeItems()
    }
    
    public func end() {
//        coordinator.end()
    }
    
    func clearLasterLayers() {
        mapRasterLayer = nil
    }
    
    func getRasterLayer() -> AWFRasterMapLayer? {
        mapRasterLayer
    }
    
    func setNewCurrentLayer(_ layer: AWFRasterMapLayer) {
        mapRasterLayer = layer
    }
    
    func didTapFilter(_ index: Int) {
        let newSelectedItem = typeItems[index]
        currentMapTypeLayer = newSelectedItem.mapLayer
        updatedTypeItems()
        state.value = .updatedOverlay(currentMapTypeLayer)
    }
    
    // MARK: - Private properties
    
    private unowned let coordinator: MapCoordinator
    private let homeLocation: WNLocation
    private let _location: WNLocation
    private var _userCurrentLocation: WNCoordinate?
    private(set) var typeItems: [TypeItem] = []
    private(set) var currentMapTypeLayer: AWFMapLayer
    private var mapRasterLayer: AWFRasterMapLayer?
    private let currentLocationService: CurrentLocationService
    private let serialQueue = DispatchQueue(label: "map.serial.queue")
    
    // MARK: - Private methods
    
    private func getUserCoordinates(
        completion: @escaping Block<CLLocation?>
    ) {
        serialQueue.async { [weak self] in
            self?.currentLocationService.getLocationIfAvailable { location in
                completion(location)
            }
        }
    }
    
    private func createData() {
        getUserCoordinates() { [weak self] locValue in
            guard let currentlocation = locValue else { return }
            self?._userCurrentLocation = WNCoordinate(
                lat: currentlocation.coordinate.latitude,
                long: currentlocation.coordinate.longitude
            )
            guard let layer = self?.currentMapTypeLayer else { return }
            self?.state.value = .started(layer)
        }
        
        typeItems = [
            .init(name: .infraredSatellite, mapLayer: .satelliteInfraredColor),
            .init(name: .humidity, mapLayer: .humidity),
            .init(name: .advisories, mapLayer: .advisories),
            .init(name: .temperatures, mapLayer: .feelsLike),
            .init(name: .winds, mapLayer: .windSpeeds),
            .init(name: .windChill, mapLayer: .windChill),
            .init(name: .visibleSatellite, mapLayer: .satelliteVisible),
            .init(name: .heatIndex, mapLayer: .heatIndex),
            .init(name: .dewPoints, mapLayer: .dewPoints),
            .init(name: .lighting, mapLayer: .lightningStrikes15Minute),
            .init(name: .radar, isSelected: true, mapLayer: .radar)
        ]
        self.state.value = .started(self.currentMapTypeLayer)
    }
    
    private func updatedTypeItems() {
        self.typeItems = typeItems.map {
                TypeItem(name: $0.name,
                         isSelected: $0.mapLayer == currentMapTypeLayer ? true : false,
                         mapLayer: $0.mapLayer
                )
        }
    }
    
    init(location: WNLocation,
         homeLocation: WNLocation,
         flow: MapRouteFlow,
         coordinator: MapCoordinator,
         currentLocationService: CurrentLocationService = CurrentLocationServiceImpl()) {
        self.currentMapTypeLayer = flow == .home ? .radar : .lightningStrikes15Minute
        self._location = location
        self.homeLocation = homeLocation
        self.coordinator = coordinator
        self.state = GTObservable(.idle)
        self.currentLocationService = currentLocationService
    }
}
