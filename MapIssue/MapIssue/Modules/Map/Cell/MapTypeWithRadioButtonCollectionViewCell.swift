//
//  MapTypeWithRadioButtonCollectionViewCell.swift
//  WeatherNation
//
//  Created by solodkiy on 10.04.2022.
//

import UIKit

class MapTypeWithRadioButtonCollectionViewCell: BaseCollecitionViewCell<MapViewModel.TypeItem> {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var radioButton: RadioButton!
    
    // MARK: - Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        configureUI()
    }
                   
    override func layoutSublayers(of layer: CALayer) {
        super.layoutSublayers(of: layer)
        configureUI()
    }
    
    // MARK: - Public methods
    
    override func configure(with model: MapViewModel.TypeItem) {
        titleLabel.text = model.name.name
        radioButton.isSelected = model.isSelected
    }
    
    // MARK: - Private methods
    
    private func configureUI() {
        backgroundColor = .clear
        titleLabel.font = .systemFont(ofSize: 14)
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .white
    }
    
}
