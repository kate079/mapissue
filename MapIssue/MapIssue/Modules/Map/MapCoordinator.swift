//
//  MapCoordinator.swift
//  WeatherNation
//
//  Created by solodkiy on 06.04.2022.
//

import UIKit
import CoreLocation

public enum MapRouteFlow {
    case home
    case burger
}

final class MapCoordinator: BaseCoordinator<Void> {

    override func start() {
        let viewModel = MapViewModel(location: location!,
                                     homeLocation: location!,
                                     flow: flowType!,
                                     coordinator: self)
        let controller = MapViewController(viewModel: viewModel)
        controller.modalPresentationStyle = .custom
        presented = controller
        navigationController.present(controller, animated: true, completion: nil)
    }

    public func end() {
        onComplete?(())
        presented.dismiss(animated: true, completion: nil)
    }

    // MARK: - Private methods

    // MARK: - Private properties

    private unowned let navigationController: UINavigationController
    private var presented: UIViewController!
    private var location: WNLocation?
    private var flowType: MapRouteFlow?

    // MARK: - Init

    init(navigationController: UINavigationController,
         location: WNLocation?,
         flowType: MapRouteFlow? = .home) {
        self.navigationController = navigationController
        self.location = location
        self.flowType = flowType
        self.navigationController.isNavigationBarHidden = true
    }

}
