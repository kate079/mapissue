//
//  MapViewController.swift
//  WeatherNation
//
//  Created by solodkiy on 06.04.2022.
//

import UIKit
import MapKit
import Kingfisher
import AerisWeatherKit
import AerisMapKit

class MapViewController: BaseViewController<MapViewModel> {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var mapContainerView: UIView!
    @IBOutlet private weak var legendContainerView: UIView!
    @IBOutlet private weak var loadingView: ActivityLoadingView!
    @IBOutlet private weak var errorLabel: UILabel!
    @IBOutlet private weak var backButton: RoundButton!
    @IBOutlet private weak var playButton: RoundButton!
    @IBOutlet private weak var playTimer: UILabel!
    @IBOutlet private weak var timeButton: RoundButton!
    @IBOutlet private weak var timerView: UIView!
    @IBOutlet private weak var appearanceButton: RoundButton!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var timeContainerView: UIView!
    @IBOutlet private weak var timeLabel: UILabel!
    @IBOutlet private weak var pastRadioButton: RadioButton!
    @IBOutlet private weak var futureRadioButton: RadioButton!
    @IBOutlet private weak var forecastContainerView: UIView!
    
    // MARK: - Private properties
    
    private let advisories = UIImageView(image: UIImage(named: "advisories"))
    private var weatherMap: AWFWeatherMap!
    private var legendView: AWFLegendView!
    private let config = AWFWeatherMapConfig()
    private var isShowTypeSettings: Bool = false {
        didSet {
            appearanceButton.selfType = self.isShowTypeSettings ? .cancel : .appearance
            UIView.animate(withDuration: 0.5, animations: {
                self.collectionView.layer.opacity = self.isShowTypeSettings ? 1.0 : 0.0
            })
        }
    }
    private var shouldShowTimeView: Bool = false {
        didSet {
            UIView.animate(withDuration: 0.5, animations: {
                self.timeContainerView.layer.opacity = self.shouldShowTimeView ? 1.0 : 0.0
            })
        }
    }
    
    // MARK: - Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        setupButtons()
        configureCollectionView()
        viewModel.start()
    }
        
    override func observeViewModelState() {
        viewModel.state.observe(self) { [weak self] result in
            guard let self = self else { return }
            switch result.newValue {
            case .loading:
                self.loadingView.startAnimating()
            case let .started(currentLayer):
                self.collectionView.reloadData()
                self.setNewLayer(currentLayer)
                self.loadingView.stopAnimating()
                self.addUserLocationPin()
            case .failure:
                self.errorLabel.isHidden = false
                self.loadingView.stopAnimating()
            case .idle:
                self.loadingView.stopAnimating()
            case let .updatedOverlay(currentLayer):
                self.collectionView.reloadData()
                self.setNewLayer(currentLayer)
            }
        }
    }
    
    // MARK: - Private methods
    private func setNewLayer(_ layerType: AWFMapLayer) {
        playButton.isHidden = (layerType == .lightningStrikes15Minute || layerType == .radar) ? false : true
        timerView.isHidden = (layerType == .lightningStrikes15Minute || layerType == .radar) ? false : true

        switch layerType {
        case .lightningStrikes15Minute: timerView.layer.maskedCorners = [.layerMaxXMinYCorner,
                                                                         .layerMaxXMaxYCorner,
                                                                         .layerMinXMaxYCorner,
                                                                         .layerMinXMinYCorner]
        case .radar: timerView.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner]
        default: break
        }
        timeButton.isHidden = layerType == .radar ? false : true
        forecastContainerView.isHidden = layerType == .radar ? false : true

        updatePlayButtonState(isPlaying: false)

        if let oldLayer = viewModel.getRasterLayer() {
            weatherMap.amp.removeRasterLayer(oldLayer)
            legendView.removeLegend(forLayerType: oldLayer.layerType)
        }

        viewModel.clearLasterLayers()

        if layerType == .advisories {
            advisories.isHidden = false
        } else {
            advisories.isHidden = true
            legendView.addLegend(forLayerType: layerType)
        }

        let newLayer = AWFRasterMapLayer(layerType: layerType)
        newLayer.alpha = layerType == .satelliteVisible ? 0.5 : 1.0
        newLayer.includeTextDataIfPossible()
        newLayer.isMetric = viewModel.isMetric()
        weatherMap.amp.addRasterLayer(newLayer)
        viewModel.setNewCurrentLayer(newLayer)
        setupConfig(layerType)
    }
    
    private func addUserLocationPin() {
        let annotation = MKPointAnnotation()
        annotation.coordinate = viewModel.userCurrentLocation
        weatherMap.strategy.addAnnotation(annotation)
    }
    
    private func configureUI() {
        configureMap()
        setupLegendView()
        setupErrorLabel()
        overrideUserInterfaceStyle = .light
        mapContainerView.addSubview(weatherMap.weatherMapView)
        weatherMap.weatherMapView.translatesAutoresizingMaskIntoConstraints = false
        placeView(weatherMap.weatherMapView, into: mapContainerView)
        timerView.layer.cornerRadius = 22
    }
    
    private func setupConfig(_ newLayer: AWFMapLayer) {
        config.isMetric = viewModel.isMetric()
        config.animationEnabled = true
        config.automaticallyStartAnimationOnLoad = false
        config.maximumIntervalsForAnimation = 15
        config.animationDuration = 10
        config.refreshInterval = 30

        if pastRadioButton.isSelected {
            config.timelineStartOffsetFromNow = -2 * AWFHourInterval
            config.timelineEndOffsetFromNow = 0
        } else {
            config.timelineStartOffsetFromNow = 0
            config.timelineEndOffsetFromNow = 2 * AWFHourInterval
        }
    }
    
    private func configureCollectionView() {
        collectionView.dataSource = self
        collectionView.delegate = self
        collectionView.backgroundColor = UIColor(named: "prussia1")
        collectionView.layer.cornerRadius = 8
        collectionView.registerNib(for: MapTypeWithRadioButtonCollectionViewCell.self)
        collectionView.collectionViewLayout = createLayout()
    }
    
    private func setupButtons() {
        setupButtonsType()
        collectionView.layer.opacity = 0.0
        timeContainerView.layer.opacity = 0.0
        pastRadioButton.radioButtonStack = [futureRadioButton]
        futureRadioButton.radioButtonStack = [pastRadioButton]
        pastRadioButton.setSelected()
        
        backButton.addTarget(self, action: #selector(backButtonAction(sender:)), for: .touchUpInside)
        appearanceButton.addTarget(self, action: #selector(appearanceButtonAction(sender:)), for: .touchUpInside)
        playButton.addTarget(self, action: #selector(playAnimation(sender:)), for: .touchUpInside)
        timeButton.addTarget(self, action: #selector(timeAction(sender:)), for: .touchUpInside)
        pastRadioButton.addTarget(self, action: #selector(timeRadioButtonAction(sender:)), for: .touchUpInside)
        futureRadioButton.addTarget(self, action: #selector(timeRadioButtonAction(sender:)), for: .touchUpInside)
    }
    
    private func configureMap() {
        weatherMap = AWFWeatherMap(mapType: .apple, config: config)
        weatherMap.delegate = self
        weatherMap.amp.delegate = self
        weatherMap.mapViewDelegate = self
        weatherMap.enableAutoRefresh()
        weatherMap.animationRepeatCount = 15

        if let mapView = self.weatherMap.mapView as? MKMapView {
            let coordinate = CLLocationCoordinate2D(
                latitude: viewModel.location.coordinate.lat,
                longitude: viewModel.location.coordinate.long
            )
            let region = MKCoordinateRegion(center: coordinate,
                                            latitudinalMeters: CLLocationDistance(exactly: 45000)!,
                                            longitudinalMeters: CLLocationDistance(exactly: 45000)!)
            mapView.setRegion(mapView.regionThatFits(region), animated: true)
        }
    }
    
    private func setupLegendView() {
        legendView = AWFLegendView()
        legendView.showsCloseIndicator = false
        legendView.translatesAutoresizingMaskIntoConstraints = false
        legendView.isMetric = viewModel.isMetric()
        legendContainerView.addSubview(legendView)
        legendContainerView.addSubview(advisories)
        advisories.isHidden = true
        placeView(legendView, into: legendContainerView)
        legendView.alpha = 0.9
        legendView.layer.cornerRadius = 8
    }
    
    private func startLoadPlayAnimation() {
        if weatherMap.timeline.isReady {
            startAnimationAction()
        } else {
            stopAnimationAction()
            loadingView.startAnimating()
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
                self?.startLoadPlayAnimation()
            }
        }
    }
    
    private func startAnimationAction() {
        weatherMap.startAnimating(fromTime: weatherMap.timeline.fromTime ?? Date())
        weatherMap.timeline.start()
    }
    private func stopAnimationAction() {
        weatherMap.pauseAnimation()
        weatherMap.timeline.pause()
    }
    
    private func updatePlayButtonState(isPlaying: Bool) {
        playButton.selfType = isPlaying ? .pause : .play
    }
    
    @objc private func timeAction(sender: Any) {
        shouldShowTimeView = !shouldShowTimeView
        isShowTypeSettings = false
    }
    
    @objc private func timeRadioButtonAction(sender: RadioButton) {
        shouldShowTimeView = false
        if sender === futureRadioButton {
            timeLabel.text = "Future"
            futureRadioButton.setSelected()
            config.timelineStartOffsetFromNow = 0
            config.timelineEndOffsetFromNow = 2 * AWFHourInterval
        } else {
            timeLabel.text = "Past"
            pastRadioButton.setSelected()
            config.timelineStartOffsetFromNow = -2 * AWFHourInterval
            config.timelineEndOffsetFromNow = 0
        }
        setNewLayer(.radar)
    }
    
    @objc private func playAnimation(sender: Any) {
        if playButton.selfType == .play {
            startLoadPlayAnimation()
        } else {
            stopAnimationAction()
        }
    }
    
    @objc private func backButtonAction(sender: Any) {
        viewModel.end()
    }
    
    @objc private func appearanceButtonAction(sender: Any) {
        shouldShowTimeView = false
        isShowTypeSettings = !isShowTypeSettings
    }
}

// MARK: - UICollectionViewDelegate

extension MapViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didTapFilter(indexPath.row)
        appearanceButton.selfType = .appearance
        isShowTypeSettings = !isShowTypeSettings
    }
}

// MARK: - CollectionView datasource

extension MapViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        self.viewModel.typeItems.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.deque(type: MapTypeWithRadioButtonCollectionViewCell.self,
                                        indexPath: indexPath)
        cell.configure(with: self.viewModel.typeItems[indexPath.row])
        return cell
    }
}

// MARK: - Extension

extension MapViewController {
    
    private func setupButtonsType() {
        backButton.selfType = .back
        appearanceButton.selfType = .appearance
        playButton.selfType = .play
        timeButton.selfType = .time
    }
    
    private func setupErrorLabel() {
        errorLabel.text = "Oops! Something went wrong"
        errorLabel.font = .systemFont(ofSize: 18, weight: .medium)
        errorLabel.textColor = .black
    }
    
    private func placeView(_ childView: UIView, into view: UIView) {
        NSLayoutConstraint.activate([
            childView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            childView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            childView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            childView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0)
        ])
    }
    
    private func createLayout() -> UICollectionViewCompositionalLayout {
        let size = NSCollectionLayoutSize(
            widthDimension: .fractionalWidth(1/3),
            heightDimension: .fractionalHeight(1)
        )
        
        let itemSize = NSCollectionLayoutSize(
            widthDimension: .fractionalHeight(1),
            heightDimension: .fractionalHeight(1)
        )
        
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let group = NSCollectionLayoutGroup.vertical(
            layoutSize: size,
            subitem: item,
            count: 4
        )
        
        let section = NSCollectionLayoutSection(group: group)
        section.orthogonalScrollingBehavior = .continuous
        section.contentInsets = NSDirectionalEdgeInsets(top: 16, leading: 16, bottom: 16, trailing: 16)
        return UICollectionViewCompositionalLayout(section: section)
    }
}

// MARK: - MKMapViewDelegate

extension MapViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView: MKAnnotationView?
        
        if let annotation = annotation as? MKPointAnnotation {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "CustomAnnotationView")
            annotationView?.image = UIImage(named: "mapLocation")
        } else if let strategy = weatherMap.strategy as? AWFAppleMapStrategy {
            annotationView = strategy.defaultAnnotationView(forAnnotation: annotation)
        }
        
        return annotationView
    }
}

// MARK: - AWFWeatherMapDelegate

extension MapViewController: AWFWeatherMapDelegate {
    func weatherMap(_ weatherMap: AWFWeatherMap, didUpdateTimelineRangeFrom fromDate: Date, to toDate: Date) {
        weatherMap.animation(AWFAnimation(start: fromDate, end: toDate), didStartAt: fromDate)
        playTimer.text = fromDate.dToString()
    }
    
    func weatherMapDidStartAnimating(_ weatherMap: AWFWeatherMap) {
        updatePlayButtonState(isPlaying: true)
        loadingView.stopAnimating()
    }
    
    func weatherMapDidStopAnimating(_ weatherMap: AWFWeatherMap) {
        updatePlayButtonState(isPlaying: false)
    }
    
    func weatherMapDidResetAnimation(_ weatherMap: AWFWeatherMap) {
        updatePlayButtonState(isPlaying: false)
        loadingView.stopAnimating()
        stopAnimationAction()
        if !weatherMap.isLoadingAnimation {
            weatherMap.loadAnimationIfNeeded()
        }
    }
    
    func weatherMap(_ weatherMap: AWFWeatherMap, animationDidUpdateTo date: Date) {
        guard weatherMap.isAnimating, weatherMap.timeline.isAnimating else {
            updatePlayButtonState(isPlaying: false)
            return
        }
        playTimer.text = date.dToString()
        loadingView.stopAnimating()
    }
    
    func weatherMapDidCancelLoadingAnimationData(_ weatherMap: AWFWeatherMap) {
        updatePlayButtonState(isPlaying: false)
        loadingView.stopAnimating()
    }
}

// MARK: - AWFAmpTileSourceProviderDelegate

extension MapViewController: AWFAmpTileSourceProviderDelegate {
    func ampSourceProvider(_ provider: AWFAmpTileSourceProvider, needsSourceAddedToMap source: AWFTileSource) {
        weatherMap.addSource(source)
    }
    
    func ampSourceProvider(_ provider: AWFAmpTileSourceProvider, needsSourceRemovedFromMap source: AWFTileSource) {
        weatherMap.removeSource(source)
    }
}
