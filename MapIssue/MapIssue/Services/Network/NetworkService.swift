//
//  NetworkService.swift
//  WeatherNation
//
//  Created by Alex Hafros on 04.11.2021.
//

import Foundation

protocol NetworkService: AnyObject {
    @discardableResult
    func request<R: NetworkRequest>(
        _ request: R,
        completionHandler: @escaping (Result<Data?, Error>) -> Void
    ) -> URLSessionDataTask

    @discardableResult
    func uploadRequest<R: NetworkRequest>(
        _ request: R,
        completionHandler: @escaping (Result<Data?, Error>) -> Void
    ) -> URLSessionDataTask
}
