//
//  NetworkRequest.swift
//  WeatherNation
//
//  Created by Alex Hafros on 04.11.2021.
//

import Foundation

protocol NetworkRequest {
    associatedtype Body: Encodable

    var url: URL { get }
    /// HTTP Method
    var method: HTTPMethod { get }
    /// Provided custom headers
    var headers: [String: String] { get }
    /// Provided custom params
    var params: [String: String] { get }
    /// Provided body object
    var body: Body? { get }
}
