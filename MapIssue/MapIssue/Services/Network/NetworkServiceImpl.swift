//
//  NetworkServiceImpl.swift
//  WeatherNation
//
//  Created by Alex Hafros on 04.11.2021.
//

import Foundation

final class NetworkServiceImpl {
    private let session: URLSession
    private let encoder: JSONEncoder

    init(session: URLSession, encoder: JSONEncoder) {
        self.session = session
        self.encoder = encoder
    }
}

extension NetworkServiceImpl: NetworkService {

    @discardableResult
    func request<R>(
        _ request: R,
        completionHandler: @escaping (Result<Data?, Error>) -> Void
    ) -> URLSessionDataTask where R: NetworkRequest {
        let urlRequest = createURLRequest(from: request)

        let task = session.dataTask(with: urlRequest) { data, response, error  in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            completionHandler(.success(data))
        }
        defer { task.resume() }
        return task
    }

    @discardableResult
    func uploadRequest<R>(
        _ request: R,
        completionHandler: @escaping (Result<Data?, Error>) -> Void
    ) -> URLSessionDataTask where R: NetworkRequest {
        let urlRequest = createURLRequest(from: request, isUploadRequest: true)
        var uploadData: Data?
        if let data = request.body as? Data {
            uploadData = data
        }
        let task = session.uploadTask(with: urlRequest, from: uploadData) {
            data, response, error in
            if let error = error {
                completionHandler(.failure(error))
                return
            }
            completionHandler(.success(data))
        }
        defer { task.resume() }
        return task
    }
}

private extension NetworkServiceImpl {
    func createURLRequest<R>(
        from request: R,
        isUploadRequest: Bool = false
    ) -> URLRequest where R: NetworkRequest {
        var components = URLComponents(url: request.url, resolvingAgainstBaseURL: true)!
        var queryItems = [URLQueryItem]()
        for (key, value) in request.params {
            queryItems.append(URLQueryItem(name: key, value: value))
        }
        components.queryItems = queryItems
        var urlRequest: URLRequest = URLRequest(url: components.url!)
        urlRequest.httpMethod = request.method.rawValue
        if !isUploadRequest, let body = request.body {
            urlRequest.httpBody = try? encoder.encode(body)
        }
        for (key, value) in request.headers {
            urlRequest.addValue(value, forHTTPHeaderField: key)
        }
        return urlRequest
    }
}
