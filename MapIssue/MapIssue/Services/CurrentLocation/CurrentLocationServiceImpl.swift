//
//  LocationServiceImpl.swift
//  WeatherNation
//
//  Created by Alex Hafros on 05.11.2021.
//

import CoreLocation

final class CurrentLocationServiceImpl: NSObject { }

extension CurrentLocationServiceImpl: CurrentLocationService {

    // MARK: - Public methods

    func getLocationIfAvailable(handler: @escaping (CLLocation?) -> Void) {
        handler(CLLocation(latitude: 37.773972, longitude: -122.431297))
    }
}
