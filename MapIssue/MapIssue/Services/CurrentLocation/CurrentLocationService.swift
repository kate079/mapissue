//
//  LocationService.swift
//  WeatherNation
//
//  Created by Alex Hafros on 05.11.2021.
//

import CoreLocation

protocol CurrentLocationService: AnyObject {
    func getLocationIfAvailable(handler: @escaping (CLLocation?) -> Void)
}
