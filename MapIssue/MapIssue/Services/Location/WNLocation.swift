//
//  WNLocation.swift
//  WeatherNation
//
//  Created by Alex Hafros on 02.11.2021.
//

import Foundation

public struct WNLocation: Codable, Equatable {
    let coordinate: WNCoordinate
    
    private enum CodingKeys: String, CodingKey {
        case coordinate = "loc"
    }
    
    init(coordinate: WNCoordinate) {
        self.coordinate = coordinate
    }
    
    public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        coordinate = try container.decode(WNCoordinate.self, forKey: .coordinate)
    }
}
