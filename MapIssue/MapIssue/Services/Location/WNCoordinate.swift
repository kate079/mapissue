//
//  WNCoordinate.swift
//  WeatherNation
//
//  Created by Alex Hafros on 02.11.2021.
//

import Foundation

struct WNCoordinate: Codable, Equatable {
    let lat: Double
    let long: Double
}
