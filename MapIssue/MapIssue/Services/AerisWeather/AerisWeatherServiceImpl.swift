//
//  AerisWeatherServiceImpl.swift
//  WeatherNation
//
//  Created by natife on 07.07.2022.
//

import Foundation
import AerisWeatherKit

class AerisWeatherServiceImpl {
    enum Error: Swift.Error {
        /// Decoding error.
        case decodingError
        /// Internal error.
        case `internal`
    }

    private let networkService: NetworkService
    private let decoder: JSONDecoder

    init(
        networkService: NetworkService,
        decoder: JSONDecoder
    ) {
        self.networkService = networkService
        self.decoder = JSONDecoder() // decoder
    }

    fileprivate func decodeResult<T: Decodable>(
            incomingResult: Result<Data?, Swift.Error>) -> Result<[T], Error> {

        switch incomingResult {
        case .success(let data) where data != nil:
            if let arrayResponse = try? decoder.decode(NetworkResponse<[T]>.self, from: data!) {
                return .success(arrayResponse.response)
            }
            if let singleResponse = try? decoder.decode(NetworkResponse<T>.self, from: data!) {
                return .success([singleResponse.response])
            }
            return .failure(.decodingError)
        default:
            return .failure(.internal)
        }
    }
}

extension AerisWeatherServiceImpl: AerisWeatherService {
    func start() {
        AerisWeather.start(withApiKey: Environment.aerisApiKey,
                           secret: Environment.aerisSecretKey)

        #if DEBUG
        AerisWeather.sharedInstance().debugMode = true
        #else
        AerisWeather.sharedInstance().debugMode = false
        #endif
    }

    func advisoriesLightnings(
        location: WNLocation,
        distance: Int,
        completionHandler: @escaping (Result<[WNAdvisoriesLightningsRequest.Response], Error>) -> Void
    ) {
        let request = WNAdvisoriesLightningsRequest(location: location, distance: distance)
        networkService.request(request) { [unowned self] result in
            completionHandler(
                self.decodeResult(incomingResult: result)
            )
        }
    }
}
