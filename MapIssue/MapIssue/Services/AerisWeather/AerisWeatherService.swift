//
//  AerisWeatherService.swift
//  WeatherNation
//
//  Created by natife on 07.07.2022.
//

import Foundation

protocol AerisWeatherService: AnyObject {
    func start()
    func advisoriesLightnings(
        location: WNLocation,
        distance: Int,
        completionHandler: @escaping (Result<[WNAdvisoriesLightningsRequest.Response],
                                      AerisWeatherServiceImpl.Error>) -> Void
    )
}
